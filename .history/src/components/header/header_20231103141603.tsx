import { Box, Flex, Link } from "@chakra-ui/react";
import { Link as ReactRouterLink } from "react-router-dom";

const Header = () => {
  return (
    <Box as="header" bg="gray.100" w="100%" p={4} boxShadow="sm">
      <Flex align="center" justify="space-between" maxW="1200px" mx="auto">
        {/* Logo ou titre ici */}
        <Text fontSize="2xl" fontWeight="bold" as={ReactRouterLink} to="/">
          Alain Térieur & Extérieur
        </Text>

        {/* Navigation Links */}
        <Flex gap="4">
          <Link as={ReactRouterLink} to="/coordonnees-et-services" fontWeight="semibold" _hover={{ color: "blue.500" }}>
            COORDONNEES ET SERVICES
          </Link>
          <Link as={ReactRouterLink} to="/photos" fontWeight="semibold" _hover={{ color: "blue.500" }}>
            PHOTOS
          </Link>
          <Link as={ReactRouterLink} to="/conditions-generales" fontWeight="semibold" _hover={{ color: "blue.500" }}>
            CONDITIONS GENERALES
          </Link>
        </Flex>
      </Flex>
    </Box>
  );
}

export default Header;
