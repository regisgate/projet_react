// header.tsx
import { Box, Flex, Link, Text } from "@chakra-ui/react";
import NextLink from 'next/link';
import CoordonneesEtServices from "@/pages/coordonnees-et-services";

const Header = () => {
  return (
    <Box as="header" bg="gray.100" w="100%" p={4} boxShadow="sm">
      <Flex align="center" justify="space-between" maxW="1200px" mx="auto">
        {/* Logo ou titre ici */}
        <Text fontSize="2xl" fontWeight="bold">
          <NextLink href="/" passHref>
            <Link _hover={{ textDecoration: 'none' }}>
              Alain Térieur & Extérieur
            </Link>
          </NextLink>
        </Text>

        {/* Navigation Links */}
        <Flex gap="4">
          <NextLink href="/CoordonneesEtServices" passHref>
            <Link fontWeight="semibold" _hover={{ color: "blue.500" }}>
              COORDONNEES ET SERVICES
            </Link>
          </NextLink>
          <NextLink href="/photos" passHref>
            <Link fontWeight="semibold" _hover={{ color: "blue.500" }}>
              PHOTOS
            </Link>
          </NextLink>
          <NextLink href="/conditions-generales" passHref>
            <Link fontWeight="semibold" _hover={{ color: "blue.500" }}>
              CONDITIONS GENERALES
            </Link>
          </NextLink>
        </Flex>
      </Flex>
    </Box>
  );
}

export default Header;
