import { Box, Text, Button, Link, Image } from "@chakra-ui/react";

export default function CoordonneesEtServices() {
  return (
    <Box bgColor="#d8dcd6" p={5}>
      <header>
        {/* Menu et logo ici */}
      </header>

      <Box textAlign="center" mt={5}>
        <Text fontSize="xl" fontWeight="bold" mb={3}>
          <Link href="https://www.sitew.com/" isExternal>
            Facile, rapide et gratuit
          </Link>
        </Text>
        <Text fontSize="xl" fontWeight="bold" mb={3}>
          <Link href="https://www.sitew.com/" isExternal>
            Créez votre site maintenant
          </Link>
        </Text>
        <Button as={Link} href="https://www.sitew.com/" colorScheme="green" mt={3} isExternal>
          Je crée mon site
        </Button>
      </Box>

      <Box textAlign="center" mt={5}>
        <Text fontSize="xl" fontWeight="bold" mb={3}>
          Site gratuit créé sur
        </Text>
        <Link href="https://www.sitew.com/" isExternal>
          <Image src="https://ssl.sitew.org/images/blog/logos/2021_wide.svg" alt="SiteW Logo" />
        </Link>
      </Box>

      <footer>
        {/* Liens vers les réseaux sociaux ici */}
      </footer>
    </Box>
  );
}
