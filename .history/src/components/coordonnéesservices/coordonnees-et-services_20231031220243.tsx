import { Box, Image, Text } from "@chakra-ui/react";

export default function CoordonneesEtServices() {
  return (
    <Box>
      <header>
        {/* Menu et logo ici */}
      </header>

      <main>
        <section>
          <Image src="/path/to/image1.jpg" alt="Image de coordonnées" />
          {/* Informations de coordonnées ici */}
        </section>

        <section>
          <Image src="/path/to/image2.jpg" alt="Image de services" />
          {/* Liste des services ici */}
        </section>
      </main>

      <footer>
        {/* Liens vers les réseaux sociaux ici */}
      </footer>
    </Box>
  );
}
