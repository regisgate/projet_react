import { Box, Image, Text, Flex } from "@chakra-ui/react";

export default function CoordonneesEtServices() {
  return (
    <Box>
      <header>
        {/* Menu et logo ici */}
      </header>

      <Flex direction="column" alignItems="center">
        <Box width="1600px" height="752px" position="relative">
          <Image src="/path/to/image1.jpg" alt="Image de coordonnées" width="100%" height="100%" />
          {/* Surimpression des informations sur l'image */}
          <Flex direction="column" position="absolute" top="0" left="0" width="100%" height="100%" justifyContent="center" alignItems="center">
            <Text fontSize="2xl" fontWeight="bold" color="white">Coordonnées</Text>
            {/* Informations de coordonnées ici */}
            <Text fontSize="2xl" fontWeight="bold" color="white">Services</Text>
            {/* Liste des services ici */}
          </Flex>
        </Box>
      </Flex>

      <footer>
        {/* Liens vers les réseaux sociaux ici */}
      </footer>
    </Box>
  );
}
