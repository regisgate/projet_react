import { Box, Text, Image, Flex, VStack, HStack } from "@chakra-ui/react";

export default function CoordonneesEtServices() {
  return (
    <Box bgColor="#d8dcd6" p={5}>
      <header>
        <Flex justifyContent="space-between" alignItems="center">
          <Text fontSize="2xl" fontWeight="bold">
            Alain Térieur & Extérieur
          </Text>
          {/* Menu ici */}
        </Flex>
      </header>

      <Flex direction="column" alignItems="center" mt={10}>
        <VStack spacing={8} width="80%">
          <Box width="full" borderRadius="md" overflow="hidden">
            <Image src="/path/to/image1.png" alt="Image de coordonnées et services" />
          </Box>

          <VStack spacing={4} alignItems="start">
            <HStack spacing={5}>
              <Text fontSize="xl" fontWeight="bold">Coordonnées:</Text>
              {/* Informations de coordonnées ici */}
            </HStack>
            <HStack spacing={5}>
              <Text fontSize="xl" fontWeight="bold">Services:</Text>
              {/* Liste des services ici */}
            </HStack>
          </VStack>
        </VStack>
      </Flex>

      <footer>
        {/* Liens vers les réseaux sociaux ici */}
      </footer>
    </Box>
  );
}
