// pages/coordonnees-et-services.tsx
import { Box, Container, Heading, Text } from "@chakra-ui/react";
import Head from 'next/head';
import Header from '@/components/header/header';

const CoordonneesEtServices = () => {
  return (
    <>
      <Head>
        <title>Coordonnées et Services - Alain Térieur & Extérieur</title>
        <meta name="description" content="Découvrez nos coordonnées et services." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Container maxW="container.xl" py={10}>
        <Heading as="h1" mb={6}>
          Coordonnées et Services
        </Heading>
        <Text>
          {/* Ici, vous pouvez ajouter le contenu de votre page en fonction de l'image fournie */}
        </Text>
        {/* Ajoutez d'autres composants Chakra UI ici pour construire votre page en fonction de l'image */}
      </Container>
    </>
  );
}

export default CoordonneesEtServices;
