import { Box, Text, Image, Flex, VStack } from "@chakra-ui/react";

function MainPage() {
  return (
    <Box bgColor="#fefefe" p={5}>
      {/* En-tête */}
      <header>
        {/* Ici, vous pouvez ajouter des éléments d'en-tête comme le logo, le menu, etc. */}
      </header>

      {/* Contenu principal */}
      <Flex direction={["column", "row"]} mt={10}>
        {/* Section 1 */}
        <Box p={5} align="left" flex="1">
          <Image src="/path/to/contenu1.PNG" alt="Image de la section 1" mb={4} />
          <Text color="red" fontSize="xl">
            Pour privés, entreprises, professions libérales, écoles, zonings,...
          </Text>
        </Box>

        {/* Section 2 */}
        <Box p={5} align="left" flex="1">
          <Text fontSize="xl">
            Très bonne année 2022 et prenez soin de vous
          </Text>
        </Box>
      </Flex>

      {/* Section 3 */}
      <Box p={5} align="left" mt={6}>
        <Image src="/path/to/contenu2.PNG" alt="Image de la section 3" />
      </Box>

      {/* Section 4 */}
      <Box p={5} align="left" mt={6}>
        <Image src="/path/to/contenu3.PNG" alt="Image de la section 4" />
      </Box>

      {/* Pied de page */}
      <footer>
        {/* Ici, vous pouvez ajouter des éléments de pied de page comme des liens, des mentions légales, etc. */}
      </footer>
    </Box>
  );
}

export default MainPage;
