import { Box, Container, Text, VStack, Image, HStack, Icon } from '@chakra-ui/react';
import { FaHome } from 'react-icons/fa'; // Exemple d'utilisation d'une icône
import Head from 'next/head';
import Header from '@/components/header/header';

const CoordonneesEtServices = () => {
  return (
    <>
      <Header />
      <Container maxW="container.xl" py={10}>
        <VStack spacing={8}>
          {/* Première Section */}
          <HStack align="start" spacing={10}>
            <Box w="50%">
              <Image src="/path/to/your/image.jpg" alt="Image Description" />
              <Text color="red" textDecoration="underline">
                Pour privés, entreprises, professions libérales, écoles, zonings,...
              </Text>
            </Box>

            {/* Deuxième Section */}
            <Box w="50%">
              <Text color="red" textDecoration="underline">
                Très bonne année 2022 et prenez soin de vous
              </Text>
            </Box>
          </HStack>

          {/* Troisième Section */}
          <Text color="blue">
            Contrat à l'année
            Pour quelques fois
            Pour une fois
            C'est vous qui décidez
          </Text>

          {/* Quatrième Section */}
          <HStack align="start">
            <Icon as={FaHome} w={6} h={6} />
            <Text>
              Coordonnées
              Alain DE TURCK
              Rue Trieu Braibant, n°13
              6230 Pont-à-Celles
              N° de TVA : BE0661.684.708
              Gsm : 0468/37-05-89
              E mail : alain.hommeatoutfaire@gmail.com
              Facebook : alain térieur & extérieur
            </Text>
          </HStack>
        </VStack>
      </Container>
    </>
  );
};

export default CoordonneesEtServices;
