import { Box, Container, Text, VStack, Image, HStack, Icon } from '@chakra-ui/react';
import { FaHome } from 'react-icons/fa'; // Assuming react-icons has been installed
import Head from 'next/head';
import Header from '@/components/header/header';

const CoordonneesEtServices = () => {
  return (
    <>
      <Head>
        <title>Coordonnées et Services - Alain Térieur & Extérieur</title>
        <meta name="description" content="Découvrez nos coordonnées et services." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header/>
      <Container bgColor="#ADD8E6" maxW="container.xl" py={10}>
        <VStack spacing={8}>
          
          <HStack align="start" spacing={10}>
            <Box  w="50%">
              <Image src="/images/image.jpg" alt="Image Description" />
              <Text color="red" textDecoration="underline">
                Pour privés, entreprises, professions libérales, écoles, zonings,...
              </Text>
            </Box>

            <Box w="50%">
              <Text color="red" textDecoration="underline">
                Très bonne année 2022 et prenez soin de vous
              </Text>
            </Box>
          </HStack>

          
          <Text color="blue">
            Contrat à l'année
            Pour quelques fois
            Pour une fois
            C'est vous qui décidez
          </Text>
          <HStack align="start">
            <Icon as={FaHome} w={6} h={6} />
            <Text>
              Coordonnées
              Alain DE TURCK
              Rue Trieu Braibant, n°13
              6230 Pont-à-Celles
              N° de TVA : BE0661.684.708
              Gsm : 0468/37-05-89
              E mail : alain.hommeatoutfaire@gmail.com
              Facebook : alain térieur & extérieur
            </Text>
          </HStack>
        </VStack>
      </Container>
    </>
  );
};

export default CoordonneesEtServices;
