import { Box, Container, Text, VStack, Heading } from '@chakra-ui/react';
import Head from 'next/head';

const ConditionsGenerales = () => {
  return (
    <>
      <Head>
        <title>Conditions Générales - Alain Térieur & Extérieur</title>
        <meta name="description" content="Conditions générales de service." />
      </Head>
      <Container maxW="container.xl" py={10}>
        <VStack spacing={8}>
          <Heading as="h1" size="xl">Conditions Générales</Heading>
          <Text>
                1. Sauf convention contraire et expresse, seules sont d’application les conditions générales et particulières du présent devis, bon de commande ou facture. 

                2. Les devis et offres de prix sont basés sur les prix actuels des salaires, matériaux et services. Si ceux-ci subissent des modifications indépendantes de la volonté de l’entrepreneur, celui-ci se réserve le droit d’adapter proportionnellement les prix. Le client a le droit de résilier la convention dans les huit jours après que la modification de prix lui a été signifiée. 

                3. Nos offres sont valables durant 30 jours. 
          </Text>
                4. Les commandes ne sont considérées comme définitives qu’après la signature du devis ou bon de commande par la personne autorisée ou par le versement d’un acompte défini par l’entreprise. En cas de désistement du client, une indemnité de 15% du montant du contrat restera acquise à l’entrepreneur à titre de dédommagement. Une indemnité du même montant sera due au client, si la résiliation du contrat est imputable à l’entrepreneur, sauf cas de force majeure. 

                5. Le client est entièrement responsable de l’obtention des permis urbanistiques éventuellement requis pour les travaux commandés. 

                6. Les délais d’exécutions éventuels se trouvent suspendus ou prorogés en cas de force majeure, gel, pluies, intempéries, difficultés d’approvisionnement, maladie … Ils sont de même suspendus ou prorogés du nombre de jours de retard dans l’échelle des paiements et ce, sans indemnité. 

                7. Toute réclamation sera faite par lettre recommandée au plus tard 8 jours après réception de la facture. Sauf convention contraire et écrite, nos travaux sont payables au fur et à mesure de l’état d’avancement de l’entreprise, et au plus tard 15 jours dates de facture. 

                8. Nos prix s’entendent hors TVA. Seront appliqués, les taux de TVA en vigueur au moment de la facturation. 

                9. Si, au cours des travaux, le client nous charge de travaux en régie, ceux-ci seront consignés sur des bordereaux spéciaux, signés par le client pour accord. 

                10. Au fur et à mesure de l’achèvement de chaque poste du devis, l’entretien normal de même que les réfections éventuelles : par exemple tassement de remblais, dégâts provoqués par des pluies torrentielles, orages, … seront facturés au client au taux des travaux en régie. Les postes seront considérés comme reçus. 
          <Text>
                11. En aucun cas, l’entrepreneur ne pourra être rendu responsable des mauvaises herbes qui pourraient apparaître dans les pelouses nouvellement semées. 

                12. En aucun cas l’entrepreneur ne pourra être rendu responsable des éventuels matériaux mis à disposition pour le client. Seul le client en sera responsable (qualité, usure, tailles …) Nous ne sommes pas responsable des tassements de sol, remblais, chapes .... effectués par un autre entrepreneur ou par le client. 

                13. Le client accepte automatiquement le nettoyage du terrain avant tout début de chantier payable au prix fixé par l’entrepreneur. 

                14. En aucun cas, l’entrepreneur ne pourra être rendu responsable de la plantation et croissance des nouvelles haies, parterres divers, pelouses, massifs .... Nous ne sommes pas responsables de la qualité des plantes, de l'arrosage, du bon entretien suivant la plantation. Nous ne sommes pas responsables des maladies dans les végétaux. "Une plante peut mourir en 48h" Nous conseillons vivement l'installation d'un arrosage automatique type Goutte à Goutte la première année de croissance. 

                15. Un plan d’implantation doit être remis à l’entrepreneur avant la mise en chantier, sans quoi le client s’expose à devoir payer les frais dus à de mauvais bornages ou alignements existants. 

                16. Aucune responsabilité ne nous incombe pour tous dégâts occasionnés à des ouvrages non signalés, et non visibles. Au cas où des obstacles non visibles et non signalés apparaissent au cours des travaux, ils donnent droit à une révision des prix pour l’extraction et l’évacuation de ces obstacles. 

                17. Une fois les travaux entamés, s’ils devaient être suspendus pour une cause quelconque indépendante de la volonté de l’entrepreneur, les frais en résultant seraient facturés au responsable de cet état de fait. 

                18. A défaut de paiement de la facture à l’échéance, les intérêts moratoires seront dus de plein droit, sans sommation ni autre mise en demeure, à un taux de 12% l’an. 

                19. A défaut de paiement d’une facture à l’échéance dans les 15 jours de sa date d’échéance, il sera dû, en outre et sans mise en demeure, une indemnité forfaitaire de 15% du montant de la facture, sans que cette indemnité puisse être inférieure à 50€. 

 

En cas de contestation, le tribunal de Charleroi est seul compétent.
          </Text>
          <Text>
            Duis aute irure dolor in reprehenderit in voluptate velit esse 
            cillum dolore eu fugiat nulla pariatur.
          </Text>
          {/* ... more content */}
        </VStack>
      </Container>
    </>
  );
};

export default ConditionsGenerales;
