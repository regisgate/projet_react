import { Box, Container, Text, VStack, Heading } from '@chakra-ui/react';
import Head from 'next/head';

const ConditionsGenerales = () => {
  return (
    <>
      <Head>
        <title>Conditions Générales - Alain Térieur & Extérieur</title>
        <meta name="description" content="Conditions générales de service." />
      </Head>
      <Container maxW="container.xl" py={10}>
        <VStack spacing={8}>
          <Heading as="h1" size="xl">Conditions Générales</Heading>
          <Text>
                1. Sauf convention contraire et expresse, seules sont d’application les conditions générales et particulières du présent devis, bon de commande ou facture. 

                2. Les devis et offres de prix sont basés sur les prix actuels des salaires, matériaux et services. Si ceux-ci subissent des modifications indépendantes de la volonté de l’entrepreneur, celui-ci se réserve le droit d’adapter proportionnellement les prix. Le client a le droit de résilier la convention dans les huit jours après que la modification de prix lui a été signifiée. 

                3. Nos offres sont valables durant 30 jours. 
          </Text>
          4. Les commandes ne sont considérées comme définitives qu’après la signature du devis ou bon de commande par la personne autorisée ou par le versement d’un acompte défini par l’entreprise. En cas de désistement du client, une indemnité de 15% du montant du contrat restera acquise à l’entrepreneur à titre de dédommagement. Une indemnité du même montant sera due au client, si la résiliation du contrat est imputable à l’entrepreneur, sauf cas de force majeure. 

            5. Le client est entièrement responsable de l’obtention des permis urbanistiques éventuellement requis pour les travaux commandés. 

            6. Les délais d’exécutions éventuels se trouvent suspendus ou prorogés en cas de force majeure, gel, pluies, intempéries, difficultés d’approvisionnement, maladie … Ils sont de même suspendus ou prorogés du nombre de jours de retard dans l’échelle des paiements et ce, sans indemnité. 

            7. Toute réclamation sera faite par lettre recommandée au plus tard 8 jours après réception de la facture. Sauf convention contraire et écrite, nos travaux sont payables au fur et à mesure de l’état d’avancement de l’entreprise, et au plus tard 15 jours dates de facture. 

            8. Nos prix s’entendent hors TVA. Seront appliqués, les taux de TVA en vigueur au moment de la facturation. 

            9. Si, au cours des travaux, le client nous charge de travaux en régie, ceux-ci seront consignés sur des bordereaux spéciaux, signés par le client pour accord. 

            10. Au fur et à mesure de l’achèvement de chaque poste du devis, l’entretien normal de même que les réfections éventuelles : par exemple tassement de remblais, dégâts provoqués par des pluies torrentielles, orages, … seront facturés au client au taux des travaux en régie. Les postes seront considérés comme reçus. 
          <Text>
            Ut enim ad minim veniam, quis nostrud exercitation ullamco 
            laboris nisi ut aliquip ex ea commodo consequat.
          </Text>
          <Text>
            Duis aute irure dolor in reprehenderit in voluptate velit esse 
            cillum dolore eu fugiat nulla pariatur.
          </Text>
          {/* ... more content */}
        </VStack>
      </Container>
    </>
  );
};

export default ConditionsGenerales;
