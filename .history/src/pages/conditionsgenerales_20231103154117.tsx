import { Box, Container, Text, VStack, Heading } from '@chakra-ui/react';
import Head from 'next/head';

const ConditionsGenerales = () => {
  return (
    <>
      <Head>
        <title>Conditions Générales - Alain Térieur & Extérieur</title>
        <meta name="description" content="Conditions générales de service." />
      </Head>
      <Container maxW="container.xl" py={10}>
        <VStack spacing={8}>
          <Heading as="h1" size="xl">Conditions Générales</Heading>
          <Text>
            {/* Replace the content here with the actual text from your image */}
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </Text>
          {/* You can add more <Text> components for additional paragraphs */}
          <Text>
            Ut enim ad minim veniam, quis nostrud exercitation ullamco 
            laboris nisi ut aliquip ex ea commodo consequat.
          </Text>
          <Text>
            Duis aute irure dolor in reprehenderit in voluptate velit esse 
            cillum dolore eu fugiat nulla pariatur.
          </Text>
          {/* ... more content */}
        </VStack>
      </Container>
    </>
  );
};

export default ConditionsGenerales;
