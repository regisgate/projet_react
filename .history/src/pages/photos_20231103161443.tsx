import Header from '@/components/header/header';
import { Box, SimpleGrid, Image, Container } from '@chakra-ui/react';
import Head from 'next/head';

const PhotosPage = () => {
  
  const imageSources = [
    '/path/to/your/image1.jpg',
    '/path/to/your/image2.jpg',
    '/path/to/your/image3.jpg',
    
  ];

  return (
    <>
      <Head>
        <title>Photos - Alain Térieur & Extérieur</title>
        <meta name="description" content="Découvrez notre galerie de photos." />
      </Head>
      <Header />
      <Box bg="yourBackgroundColor" minH="100vh"> {/* Replace 'yourBackgroundColor' with the actual color */}
        <Container maxW="container.xl" py={10}>
          <SimpleGrid columns={[2, null, 3]} spacing={10}>
            {imageSources.map((src, index) => (
              <Box key={index} boxShadow="lg" borderRadius="md" overflow="hidden">
                <Image src={src} alt={`Photo ${index + 1}`} objectFit="cover" width="100%" />
              </Box>
            ))}
          </SimpleGrid>
        </Container>
      </Box>
    </>
  );
};

export default PhotosPage;
