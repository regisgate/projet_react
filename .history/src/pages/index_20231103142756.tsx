// Import the Header component
import Header from '@/components/header/header';

// Your existing imports
import CoordonneesEtServices from '@/components/coordonnéesservices/coordonnees-et-services';
import Head from 'next/head';

export default function Home() {
  return (

      <main>
        <CoordonneesEtServices />
      </main>
    </>
  )
}
