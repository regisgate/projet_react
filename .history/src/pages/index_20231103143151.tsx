import { Box, Container, Heading, Text, VStack } from "@chakra-ui/react";
import Head from 'next/head';
import Header from '@/components/header/header';

const CoordonneesEtServices = () => {
  // Vous devrez définir la couleur de fond réelle en fonction de votre image
  const backgroundColor = "#f0f0f0"; // exemple de couleur de fond

  return (
    <>
      <Head>
        <title>Coordonnées et Services - Alain Térieur & Extérieur</title>
        <meta name="description" content="Découvrez nos coordonnées et services." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Box bgColor={backgroundColor}>
        <Container maxW="container.xl" py={10}>
          <VStack spacing={8}>
            {/* Section 1 */}
            <Box bg="white" p={6} shadow="md" borderRadius="md">
              <Heading as="h2" size="lg" mb={4}>
                Section 1 Titre
              </Heading>
              <Text>
                Contenu de la section 1...
              </Text>
            </Box>
            {/* Section 2 */}
            <Box bg="white" p={6} shadow="md" borderRadius="md">
              <Heading as="h2" size="lg" mb={4}>
                Section 2 Titre
              </Heading>
              <Text>
                Contenu de la section 2...
              </Text>
            </Box>
            {/* D'autres sections peuvent être ajoutées ici en suivant le même modèle */}
          </VStack>
        </Container>
      </Box>
    </>
  );
}

export default CoordonneesEtServices;
