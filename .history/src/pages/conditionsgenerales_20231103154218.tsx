import { Box, Container, Text, VStack, Heading } from '@chakra-ui/react';
import Head from 'next/head';

const ConditionsGenerales = () => {
  return (
    <>
      <Head>
        <title>Conditions Générales - Alain Térieur & Extérieur</title>
        <meta name="description" content="Conditions générales de service." />
      </Head>
      <Container maxW="container.xl" py={10}>
        <VStack spacing={8}>
          <Heading as="h1" size="xl">Conditions Générales</Heading>
          <Text>
                1. Sauf convention contraire et expresse, seules sont d’application les conditions générales et particulières du présent devis, bon de commande ou facture. 

                2. Les devis et offres de prix sont basés sur les prix actuels des salaires, matériaux et services. Si ceux-ci subissent des modifications indépendantes de la volonté de l’entrepreneur, celui-ci se réserve le droit d’adapter proportionnellement les prix. Le client a le droit de résilier la convention dans les huit jours après que la modification de prix lui a été signifiée. 

                3. Nos offres sont valables durant 30 jours. 
          </Text>
          {/* You can add more <Text> components for additional paragraphs */}
          <Text>
            Ut enim ad minim veniam, quis nostrud exercitation ullamco 
            laboris nisi ut aliquip ex ea commodo consequat.
          </Text>
          <Text>
            Duis aute irure dolor in reprehenderit in voluptate velit esse 
            cillum dolore eu fugiat nulla pariatur.
          </Text>
          {/* ... more content */}
        </VStack>
      </Container>
    </>
  );
};

export default ConditionsGenerales;
